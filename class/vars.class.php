<?php
class vars {
	// Pulblic function Show Vars from POST or GET or SESSION when there are set in form
	public function show($field) {
		if(isset($_POST[$field]) && !empty($_POST[$field])) {
			return $_POST[$field];
		}
		elseif(isset($_GET[$field]) && !empty($_GET[$field])) {
			return $_GET[$field];
		}
		elseif(isset($_SESSION[$field]) && !empty($_SESSION[$field])) {
			return $_SESSION[$field];
		}
		else
			return '';
	}
	
	public function get($field) {
		if(isset($_POST[$field]) && !empty($_POST[$field])) {
			return $_POST[$field];
		}
		elseif(isset($_GET[$field]) && !empty($_GET[$field])) {
			return $_GET[$field];
		}
		elseif(isset($_SESSION[$field]) && !empty($_SESSION[$field])) {
			return $_SESSION[$field];
		}
		else
			return false;
	}
	}
?>