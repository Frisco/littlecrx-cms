<?php
/**********
author : Łukasz 'caraxes' K. &copy; 2009
**********/
class content {

	  protected $host;
	  protected $user;
	  protected $pwd;
	  protected $dbName;
	 
	 
		 function __construct($host, $user, $pwd, $dbName){
			$this->host = $host;
			$this->user = $user;
			$this->pwd = $pwd;
			$this->dbName = $dbName;
		}

	public function edit($art) {
	
		try 
			{
				$pdo = new PDO('mysql:host='.$this->host.';dbname='.$this->dbName.'', ''.$this->user.'', ''.$this->pwd.'', array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
					$sql = $pdo -> prepare("SELECT content_name, content_desc
										FROM content
										WHERE content_status = 1 AND id_content = ".$art);
						$sql -> execute();
						$dane = $sql->fetch(PDO::FETCH_ASSOC);
					
					if(!empty($dane)) {
					
					if($_SERVER['REQUEST_METHOD'] == 'POST') {
						$formval = new formValidator();
										$formval -> validateEmpty('name','Tytuł nie może być pusty !',3,100);
										
										$formval_errors_number = $formval -> checkErrors();
												if($formval_errors_number > 0)
													echo "<br />".$formval -> displayErrors();
						
											if($formval_errors_number == 0) {
												$clean = new clean();
												
												$sql = $pdo -> exec("UPDATE content SET content_name = '".$clean->czysc($_POST['name'])."', content_desc = '".$_POST['content_desc']."' WHERE id_content = '".$art."'");
												
												echo 'edycja OK<script>document.location = "menu"</script>';
												
											}
					}
						echo '<form action="" method="post">
				<p class="customer_p"><label class="customer_label">Tytuł:</label><input name="name" type="text" value="'.$dane['content_name'].'" class="customer_text" /></p><textarea class="ckeditor" cols="20" id="comment" name="content_desc" rows="10">'.$dane['content_desc'].'</textarea>
				<p class="customer_p"><input class="customer_button" value="Edytuj" type="submit" /></p></form>';
					}
					else echo '<h1>404</h1><p class="text">Nie ma takiej strony</p>';

			}
			catch(PDOException $e) 
			{
			echo 'Wystapil blad biblioteki PDO: ' . $e->getMessage();
			}
		
	}

}
	
?>
