﻿<?php
/**********
author : Łukasz 'caraxes' K. &copy; 2009
**********/
class menu {

	  protected $host;
	  protected $user;
	  protected $pwd;
	  protected $dbName;
	 
	 
		 function __construct($host, $user, $pwd, $dbName){
			$this->host = $host;
			$this->user = $user;
			$this->pwd = $pwd;
			$this->dbName = $dbName;
		}
		
	public function displayMenu() {
	
		try 
			{
				$pdo = new PDO('mysql:host='.$this->host.';dbname='.$this->dbName.'', ''.$this->user.'', ''.$this->pwd.'', array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));

						$sql = $pdo -> prepare("SELECT content_name, id_content
										FROM content
										WHERE content_status = 1 ");
						$sql -> execute();
						$dane = $sql->fetchAll(PDO::FETCH_ASSOC);
						$clean = new clean();
						echo '<ul>';
						foreach($dane as $d) {
						$a = $clean->clearPl($d['content_name']);
						
							echo '<li class="top"><a href="art_'.$d['id_content'].'_'.$a.'" class="top_link">'.$d['content_name'].'</a></li>';
						}
						echo '</ul>';
			}
			catch(PDOException $e) 
			{
			echo 'Wystapil blad biblioteki PDO: ' . $e->getMessage();
			}
		
	}

}
	
?>
